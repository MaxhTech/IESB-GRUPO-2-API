const express = require('express');
const clear = require('clear');
var bodyParser = require('body-parser');
var cors = require('cors');

// Setup
const app = express();

const port = 3001;

const professors = require('./src/routes/professors');
const cursos = require('./src/routes/cursos');
const disciplina = require('./src/routes/disciplina');
const bibliografia = require('./src/routes/bibliografia');

// App Body Config
app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Routes
app.get('/', (req, res) => {
  res.status(200).send({ success: 'Para acessar a api do PPC - Grupo 2, utilize os endpoints em: /api2' });
});

app.use('/api/professors', professors);
app.use('/api/disciplina', disciplina);
app.use('/api/cursos', cursos);
app.use('/api/bibliografia', bibliografia);


// App initializing
app.listen(port);

clear();
console.log(`\nServer listening on port ${port}...`);