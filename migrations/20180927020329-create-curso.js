'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Cursos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tipoCurso: {
        type: Sequelize.STRING
      },
      modalidade: {
        type: Sequelize.STRING
      },
      denominacaoCurso: {
        type: Sequelize.STRING
      },
      habilitacao: {
        type: Sequelize.STRING
      },
      localOferta: {
        type: Sequelize.STRING
      },
      turnoFuncionamento: {
        type: Sequelize.STRING
      },
      numVagas: {
        type: Sequelize.NUMBER
      },
      cargaHora: {
        type: Sequelize.NUMBER
      },
      regimeLetivo: {
        type: Sequelize.STRING
      },
      periodos: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Cursos');
  }
};