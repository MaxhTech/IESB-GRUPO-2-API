'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bibliografia = sequelize.define('Bibliografia', {
    titulo: DataTypes.STRING,
    autor: DataTypes.STRING,
    isbn: DataTypes.STRING,
    ano: DataTypes.NUMBER,
    editora: DataTypes.STRING
  }, {});
  Bibliografia.associate = function(models) {
    // associations can be defined here
    Bibliografia.belongsTo(models.Curso, {
      foreignKey: "bibliografia_id"
    });
    Bibliografia.belongsTo(models.Disciplina, {
      foreignKey: "bibliografia_id"
    });
  };
  return Bibliografia;
};