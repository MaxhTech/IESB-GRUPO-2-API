'use strict';
module.exports = (sequelize, DataTypes) => {
  const Curso = sequelize.define('Curso', {
    tipoCurso: DataTypes.STRING,
    modalidade: DataTypes.STRING,
    denominacaoCurso: DataTypes.STRING,
    habilitacao: DataTypes.STRING,
    localOferta: DataTypes.STRING,
    turnoFuncionamento: DataTypes.STRING,
    numVagas: DataTypes.NUMBER,
    cargaHora: DataTypes.NUMBER,
    regimeLetivo: DataTypes.STRING,
    periodos: DataTypes.STRING
  }, {});
  Curso.associate = function(models) {
    // associations can be defined here
  };
  return Curso;
};